{ system ? builtins.currentSystem or "x86_64-linux"
, ghc ? "ghc9101"
}:

let
  nix = import ./nix;
  pkgs = nix.pkgSetForSystem system {
    config = {
      allowBroken = true;
      allowUnfree = true;
    };
  };
  inherit (pkgs) lib;
  hsPkgSetOverlay = pkgs.callPackage ./nix/haskell/overlay.nix {
    inherit (nix) sources;
  };

  sources = [
    "^src.*$"
    "^.*\\.cabal$"
  ];

  base = hsPkgs.callCabal2nix "HList" (lib.sourceByRegex ./. sources) { };
  hlist-overlay = _hf: _hp: { hlist = base; };
  baseHaskellPkgs = pkgs.haskell.packages.${ghc};
  hsOverlays = [ hsPkgSetOverlay hlist-overlay ];
  hsPkgs = baseHaskellPkgs.override (old: {
    overrides =
      builtins.foldl' pkgs.lib.composeExtensions (old.overrides or (_: _: { }))
        hsOverlays;
  });

  shell = hsPkgs.shellFor {
    packages = p: [ p.hlist ];
    nativeBuildInputs = (with pkgs; [
      cabal-install
      niv
    ]);
    shellHook = ''
      export PS1='$ '
    '';
  };

  hlist = hsPkgs.hlist;
in {
  inherit hsPkgs;
  inherit ghc;
  inherit pkgs;
  inherit shell;
  inherit hlist;
}
